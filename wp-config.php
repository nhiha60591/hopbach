<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hop_bach');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#lqx9;RluE@N[>%U.wi8K?VrC2`f~fH|!k[@c1iOGrNf-WhEq9& z9zv%0Na6>{i');
define('SECURE_AUTH_KEY',  '#{7z:te[v24+Weg?O!1{`/a$6$x(jx:47gc3<L,2x+w_ qgk[r,N@L-}jV8nyL,X');
define('LOGGED_IN_KEY',    ':]7Ubn7KVd`<@el,r-wGJ{#7CKc{*S6[$Y1wTqeCk2L!eH{3#CrggX?MR/!R&NM3');
define('NONCE_KEY',        'Q_k3zk|zT5ZzGPW2<{4|KWJ8(f.=4-5V5tGg$yO@eP3QU.wS6!PP5!rQ<Q.n%{n#');
define('AUTH_SALT',        'T>$EQ8A&%fI-)wWlj+5P^h@N9Wv.oi I4zXWpZ0 I>F%0A7?G<>o9/9^$ORfYplW');
define('SECURE_AUTH_SALT', 'H1B{8[wv(8ZOpjqO8y#BUsYqt>G-O1%w&2=<(J>l[oG[HT;0gpPRrg7:?]h$PW(Z');
define('LOGGED_IN_SALT',   '[ hK7SP4Rn|zg?pQ!%VG0>s#q&R!}@b+qp;}GaE>*YX!67N&RM]o%A^YH+5(IhOK');
define('NONCE_SALT',       'eYjQHweUIyL9?rV,tGycf=VsR*[RUiUknL|9b8I7a>V44yp*YU~Tut;F?1|%5|vs');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
