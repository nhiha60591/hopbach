<?php
/**
 * Template Name: Cart page
 *
 * @package WordPress
 * @subpackage Hop Bach
 * @since Hop Bach 1.0.0
 */
?>
<?php get_header(); ?>
<?php
// Start the loop.
while ( have_posts() ) : the_post();
    the_content();
endwhile;
?>
<?php get_footer(); ?>
