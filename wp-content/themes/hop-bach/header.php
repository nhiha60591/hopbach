<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class()?>>
	<header>
		<div class="header-toolbar">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<div class="header-text">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<?php _e( 'Hỗ trợ tư vấn: ', __THEMEDOMAIN__) ?>
							<?php if(function_exists('ot_get_option') ): ?>
								<span class="top-phone-number"><?php echo ot_get_option('ho_tro_tu_van'); ?></span>
							<?php else: ?>
								<span class="top-phone-number">0935.511.833 - 0935.511.593</span>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-6">
						<?php
						if ( has_nav_menu( 'header' ) ):
							wp_nav_menu(array(
								'menu' => 'top-menu', // Do not fall back to first non-empty menu.
								'theme_location' => 'header',
								'menu_class' => 'top-menu',
								'container' => '',
								'fallback_cb' => false // Do not fall back to wp_page_menu()
							));
						else:
						?>
						<ul class="top-menu">
							<li><a href="">Máy vi tính</a></li>
							<li><a href="">Mực in</a></li>
							<li><a href="">Văn Phòng Phẩm</a></li>
							<li><a href="">Đèn LED</a></li>
							<li><a href="">Dịch vụ kỹ thuật</a></li>
						</ul>
						<?php endif ?>
					</div>
				</div>
			</div><!-- END .container -->
		</div><!-- END .header-toolbar -->
		<div class="header-banner">
			<div class="container">
				<div class="row">
					<div class="col-md-6 header-logo">
						<div class="site-logo">
							<a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="<?php bloginfo('name'); ?>" /></a>
						</div>
						<div class="site-summary">
							<p class="site-description text-uppercase"><?php bloginfo('description') ?></p>
							<?php if(is_home() || is_front_page() ): ?>
								<h1 class="site-name text-uppercase"><?php bloginfo('name'); ?></h1>
							<?php else: ?>
								<h2 class="site-name text-uppercase"><?php bloginfo('name'); ?></h2>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-4 header-search">
						<?php $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) ); ?>
						<form name="header_search" id="header-search" action="<?php echo $shop_page_url; ?>" class="header-search" method="get">
							<select name="hbprc" class="header-search-select" id="header-search-select">
								<option value="0"><?php _e( 'Tất cả danh mục', __THEMEDOMAIN__ ); ?></option>
								<?php
								$terms = get_terms( 'product_cat', array(
									'hide_empty' => false,
								) );
								foreach( $terms as $term ):
								?>
									<option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
								<?php endforeach; ?>
							</select>
							<input type="text" name="s" id="s" class="s" placeholder="<?php _e( 'Nhập từ khóa tìm kiếm', __THEMEDOMAIN__ ); ?>" />
							<input type="submit" value="<?php _e('Tìm kiếm', __THEMEDOMAIN__ ); ?>" class="header-search-icon" id="header-search-icon" />
						</form>
					</div>
					<div class="col-md-2 header-cart">
						<div class="widget_shopping_cart_content"></div>
					</div>
				</div>
			</div><!-- END .container -->
		</div><!-- END .header-banner -->
	</header>
	<nav class="navbar navbar-hop-bach">
		<div class="container">
			<div class="row">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#primary-menu" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="primary-menu">
					<ul class="nav navbar-nav">
						<?php
							$class = array('dropdown');
							$expanded = 'false';
						?>
						<li class="<?php echo implode( ' ', $class ); ?>">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="<?php echo $expanded; ?>">Danh mục sản phẩm</a>
							<?php
							if ( has_nav_menu( 'danhmucsanpham' ) ):
								wp_nav_menu(array(
									'menu' => 'danh-muc-san-pham', // Do not fall back to first non-empty menu.
									'theme_location' => 'danhmucsanpham',
									'menu_class' => 'dropdown-menu',
									'container' => '',
									'fallback_cb' => false // Do not fall back to wp_page_menu()
								));
							else:
							?>
							<ul class="dropdown-menu">
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/may-vi-tinh-icon.png" class="pr-mn-icon" alt="Máy vi tính">Máy vi tính</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/may-in-icon.png" class="pr-mn-icon" alt="Máy in">Máy in</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/den-led-icon.png" class="pr-mn-icon" alt="Đèn LED">Đèn LED</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/thuc-pham-icon.png" class="pr-mn-icon" alt="Thực phẩm">Thực phẩm</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/thoi-trang-icon.png" class="pr-mn-icon" alt="Thời trang">Thời trang</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/van-phong-pham-icon.png" class="pr-mn-icon" alt="Văn Phòng Phẩm">Văn Phòng Phẩm</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/dich-vu-icon.png" class="pr-mn-icon" alt="Dịch vụ">Dịch vụ</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/cho-thue-icon.png" class="pr-mn-icon" alt="Cho thuê">Cho thuê</a></li>
								<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/hang-thanh-ly-icon.png" class="pr-mn-icon" alt="Hàng thanh lý">Hàng thanh lý</a></li>
							</ul>
							<?php endif; ?>
						</li>
					</ul>
					<?php if ( has_nav_menu( 'primary' ) ): ?>
						<?php
						wp_nav_menu(array(
							'menu' => 'primary-menu', // Do not fall back to first non-empty menu.
							'theme_location' => 'primary',
							'menu_class' => 'nav navbar-nav',
							'container' => '',
							'fallback_cb' => false // Do not fall back to wp_page_menu()
						));
						?>
					<?php else: ?>
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">Trang chủ</a></li>
							<li><a href="#">Giới thiệu</a></li>
							<li><a href="#">Sản phẩm</a></li>
							<li><a href="#">Khuyến mãi</a></li>
							<li><a href="#">Liên lạc</a></li>
						</ul>
					<?php endif; ?>
					<ul class="nav navbar-nav socials navbar-right">
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div>
		</div><!-- /.container-fluid -->
	</nav>
