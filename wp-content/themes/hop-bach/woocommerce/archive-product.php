<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
	<div class="page-header-img">
		<img width="100%" src="<?php echo get_template_directory_uri(); ?>/assets/images/product-img-header.jpg" alt="<?php the_title(); ?>" />
	</div>
	<div class="container">
		<div class="row product-header">
			<div class="col-md-6"><?php woocommerce_breadcrumb(array('delimiter'=>'&nbsp;&gt;&nbsp;')); ?></div>
			<div class="col-md-6 text-right">
				<div class="posts_per_page">
					<form method="get" id="form-show-number">
					Show:
						<?php $posts_per_page = isset($_REQUEST['posts_per_page']) ? $_REQUEST['posts_per_page'] : 20; ?>
					<select name="posts_per_page">
						<option value="20"<?php selected($posts_per_page, 20); ?>>20</option>
						<option value="30"<?php selected($posts_per_page, 30); ?>>30</option>
						<option value="40"<?php selected($posts_per_page, 40); ?>>40</option>
					</select>
					<?php
					// Keep query string vars intact
					foreach ( $_GET as $key => $val ) {
						if ( 'posts_per_page' === $key || 'submit' === $key ) {
							continue;
						}
						if ( is_array( $val ) ) {
							foreach( $val as $innerVal ) {
								echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
							}
						} else {
							echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
						}
					}
					?>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 shop-filter">
				<form method="get" id="form-show-product-type">
				<?php
					$loaihang = isset($_REQUEST['hbprc']) ? $_REQUEST['hbprc'] : 0;
					$args = array(
						'show_option_all'    => '',
						'show_option_none'   => 'Loại hàng',
						'option_none_value'  => '-1',
						'orderby'            => 'ID',
						'order'              => 'ASC',
						'show_count'         => 0,
						'hide_empty'         => 0,
						'child_of'           => 0,
						'exclude'            => '',
						'echo'               => 1,
						'selected'           => $loaihang,
						'hierarchical'       => 0,
						'name'               => 'hbprc',
						'id'                 => 'product-type-select',
						'class'              => 'product-type-select',
						'depth'              => 0,
						'tab_index'          => 0,
						'taxonomy'           => 'product_cat',
						'hide_if_empty'      => false,
						'value_field'	     => 'term_id',
					);
					wp_dropdown_categories( $args );
				?>
				<?php
				// Keep query string vars intact
				foreach ( $_GET as $key => $val ) {
					if ( 'hbprc' === $key || 'submit' === $key ) {
						continue;
					}
					if ( is_array( $val ) ) {
						foreach( $val as $innerVal ) {
							echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
						}
					} else {
						echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
					}
				}
				?>
				</form>
				<?php woocommerce_catalog_ordering(); ?>
			</div>
			<div class="col-md-6 text-right">
				<?php woocommerce_pagination() ?>
			</div>
			<div style="clear:both"></div>
		</div>
		<div class="row">
			<?php
				/**
				 * woocommerce_before_main_content hook.
				 *
				 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
				 * @hooked woocommerce_breadcrumb - 20
				 */
				do_action( 'woocommerce_before_main_content' );
			?>

			<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			do_action( 'woocommerce_archive_description' );
			?>
		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="product-page-title">
				<div class="home-widget-icon" style="color: <?php echo $instance['color']; ?>;background-color: <?php echo $instance['color']; ?>;">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/home-may-vi-tinh-icon.png" alt="<?php echo @$instance['title']; ?>" />
				</div>
				<?php woocommerce_page_title(); ?>
			</h1>

		<?php endif; ?>

		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php wc_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_after_main_content hook.
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
		?>

		<?php
			/**
			 * woocommerce_sidebar hook.
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action( 'woocommerce_sidebar' );
		?>
		</div>
	</div><!-- END .container -->

<?php get_footer( 'shop' ); ?>
