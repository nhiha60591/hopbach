jQuery(document).ready(function($){
    var myOptions = {
        // a callback to fire whenever the color changes to a valid color
        change: function(event, ui){},
        // a callback to fire when the input is emptied or an invalid color
        clear: function() {},
        // hide the color picker controls on load
        hide: true,
        // show a group of common colors beneath the square
        // or, supply an array of colors to customize further
        palettes: true
    };
    $('.color-picker').wpColorPicker(myOptions);
    $(document).ajaxComplete(function() {
        var myOptions = {
            // a callback to fire whenever the color changes to a valid color
            change: function(event, ui){},
            // a callback to fire when the input is emptied or an invalid color
            clear: function() {},
            // hide the color picker controls on load
            hide: true,
            // show a group of common colors beneath the square
            // or, supply an array of colors to customize further
            palettes: true
        };
        $('.color-picker').wpColorPicker(myOptions);
    });
    var file_frame;
    var ID = null;
    var Image = null;
    jQuery( document ).on( 'click', '.upload_img', function( event ) {

        event.preventDefault();
        ID = jQuery(this).data('id');
        Image = jQuery(this).data('image');

        // If the media frame already exists, reopen it.
        if ( file_frame ) {
            file_frame.open();
            return;
        }

        // Create the media frame.
        file_frame = wp.media.frames.downloadable_file = wp.media({
            title: 'Chọn hình ảnh',
            button: {
                text: 'Sử dụng hình này'
            },
            multiple: false
        });

        // When an image is selected, run a callback.
        file_frame.on( 'select', function() {
            var attachment = file_frame.state().get( 'selection' ).first().toJSON();
            jQuery('#'+ID).val( attachment.id );
            jQuery('#'+Image).find( 'img' ).attr( 'src', attachment.sizes.full.url );
        });

        // Finally, open the modal.
        file_frame.open();
    });
});