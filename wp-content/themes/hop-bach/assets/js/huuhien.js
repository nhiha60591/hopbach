jQuery(document).ready(function($){
    $('.alignnone').addClass('img-responsive');


    $('.quick-buy').click(function(){
        var prid = $(this).data('id');
        var data = {
            'action': 'hb_qb',
            'product_id': prid
        };

        // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        $.post(ajax_object.ajax_url, data, function(response) {
            window.location.replace(response);
        });
        /*$('.popup-pr-title').text($(this).data('title'));
        $('.popup-pr-price .product-price').text($(this).data('price'));
        $('.popup-pr-image img').attr('src', $(this).data('image') );
        $('input[name="product_id"]').val($(this).data('id'));
        $('.overlay').show();
        $('.quick-buy-popup').fadeIn(1000);
        $("#popup-form-quick-buy").validate({
            rules: {
                full_name: "required",
                phone_number: "required",
                address: "required",
                content: "required"
            },
            messages: {
                full_name: "Vui lòng họ và tên",
                phone_number: "Vui lòng nhập số điện thoại",
                address: "Vui lòng nhập địa chỉ của bạn",
                content: "Vui lòng nhập nội dung"
            }
        });*/
        return false;
    });
    $('.popup-close').click(function(){
        $('.overlay').hide();
        $('.quick-buy-popup').hide();
        return false;
    });
    $('.overlay').click(function(){
        $('.overlay').hide();
        $('.quick-buy-popup').hide();
        return false;
    });
    $('select[name="posts_per_page"]').change(function(){
        $('#form-show-number').submit();
    });
    $('.product-type-select').change(function(){
        $( this ).closest( 'form' ).submit();
    });
});