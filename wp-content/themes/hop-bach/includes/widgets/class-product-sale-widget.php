<?php
/**
 * Created a widget to display product by category on home page
 * @Author Hien (Hamilton) H.HO
 * @Date: 5/11/2016
 * @Time: 5:34 PM
 */
/**
 * Adds Foo_Widget widget.
 */
class SaleProductWidget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'product-sidebar', // Base ID
            __( 'Product-sale', __THEMEDOMAIN__ ), // Name
            array( 'description' => __( 'Hiện sản phẩm khuyến mãi ở sidebar', __THEMEDOMAIN__ ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
        }
        ?>
        <div class="home-widget-sale-body">
            <ul class="home-widget-body-sale-products">
                <?php
                $saleProductArgs = array(
                    'post_type' => 'product',
                    'posts_per_page' => $instance['number'],
                    'meta_query' => array(
                        array(
                            'key'     => '_sale_price',
                            'value'   => array('0', ''),
                            'compare' => 'NOT IN',
                        ),
                    )
                );
                $saleProduct = new WP_Query($saleProductArgs);
                ?>
                <?php if( $saleProduct->have_posts() ): ?>
                    <?php while($saleProduct->have_posts()): $saleProduct->the_post(); global $product;?>
                        <li>
                            <div class="product-img">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php
                                    if ( has_post_thumbnail() ) {
                                        $image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
                                        $image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
                                        $image         = get_the_post_thumbnail( get_the_ID(), apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
                                            'title'	=> get_the_title( get_post_thumbnail_id() )
                                        ) );

                                        $attachment_count = count( $product->get_gallery_attachment_ids() );

                                        if ( $attachment_count > 0 ) {
                                            $gallery = '[product-gallery]';
                                        } else {
                                            $gallery = '';
                                        }

                                        echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '%s', $image ), get_the_ID() );

                                    } else {

                                        echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), get_the_ID() );

                                    }
                                    ?>
                                </a>
                            </div>
                            <div class="product-des">
                                <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                <h3 class="sale-price"><?php
                                    $display_price         = $product->get_display_price();
                                    if ( $product->get_price() > 0 ) {
                                        echo number_format($display_price, 0, ",", '.' );
                                    }
                                    ?></h3>
                            </div>
                        </li>
                    <?php endwhile; ?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                <div class="clear"></div>
            </ul>
            <div class="clear"></div>
        </div>
        <?php
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Sale' );
        $number = ! empty( $instance['number'] ) ? $instance['number'] : 4;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Tiêu đề:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Số lượng:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo esc_attr( $number ); ?>">
        </p>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : __( 'Sale' );
        $instance['number'] = ( ! empty( $new_instance['number'] ) ) ? strip_tags( $new_instance['number'] ) : 4;

        return $instance;
    }

}