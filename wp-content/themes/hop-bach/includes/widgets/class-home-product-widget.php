<?php
/**
 * Created a widget to display product by category on home page
 * @Author Hien (Hamilton) H.HO
 * @Date: 5/11/2016
 * @Time: 5:34 PM
 */
/**
 * Adds Foo_Widget widget.
 */
class HomeProductWidget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'home-product-widget', // Base ID
            __( 'Sản phẩm theo chuyên mục', __THEMEDOMAIN__ ), // Name
            array( 'description' => __( 'Hiện sản phẩm theo chuyên mục ở trang chủ', __THEMEDOMAIN__ ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget( $args, $instance ) {
        echo $args['before_widget'];
        if( $instance['icon'] == 0 || empty($instance['icon']) ){
            $iconImg = get_template_directory_uri().'/assets/images/home-may-vi-tinh-icon.png';
        }else{
            list($iconImg, $width, $height) = wp_get_attachment_image_src($instance['icon'], 'full');
        }

        $img_ads = ! empty( $instance['img_ads'] ) ? $instance['img_ads'] : 0;
        if ( $img_ads ) {
            list($image_ads, $width, $height) = wp_get_attachment_image_src($img_ads, 'full');
        } else {
            $image_ads = get_template_directory_uri().'/assets/images/may-vi-tinh.jpg';
        }
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="home-widget-header">
                        <div class="home-widget-icon" style="color: <?php echo $instance['color']; ?>;background-color: <?php echo $instance['color']; ?>;">
                            <img src="<?php echo $iconImg; ?>" alt="<?php echo @$instance['title']; ?>" />
                        </div>
                        <h2 class="home-widget-title text-uppercase"><?php echo @$instance['title']; ?></h2>
                    </div>
                    <div class="home-widget-body">
                        <?php
                            $newProductArgs = array(
                                'post_type' => 'product',
                                'posts_per_page' => 5,
                                /*'meta_query' => array(
                                    array(
                                        'key'     => '_sale_price',
                                        'value'   => array('0', ''),
                                        'compare' => 'IN',
                                    ),
                                ),*/
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field'    => 'term_id',
                                        'terms'    => $instance['product_cat'],
                                        'operator' => 'IN'
                                    ),
                                ),
                            );

                            $newProduct = new WP_Query($newProductArgs);
                        ?>
                        <div class="home-widget-body-products">
                            <?php if( $newProduct->have_posts() ): ?>
                                <?php while($newProduct->have_posts()): $newProduct->the_post(); global $product;?>
                                    <div class="col-md-15 col-md-4 col-sm-6 col-xs-12">
                                        <div class="product-image text-center">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                <span class="helper"></span>
                                                <?php
                                                    if ( has_post_thumbnail() ) {
                                                        $image_caption = get_post( get_post_thumbnail_id() )->post_excerpt;
                                                        $image_link    = wp_get_attachment_url( get_post_thumbnail_id() );
                                                        $image         = get_the_post_thumbnail( get_the_ID(), apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
                                                            'title'	=> get_the_title( get_post_thumbnail_id() )
                                                        ) );

                                                        $attachment_count = count( $product->get_gallery_attachment_ids() );

                                                        if ( $attachment_count > 0 ) {
                                                            $gallery = '[product-gallery]';
                                                        } else {
                                                            $gallery = '';
                                                        }

                                                        echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '%s', $image ), get_the_ID() );

                                                    } else {

                                                        echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), get_the_ID() );

                                                    }
                                                ?>
                                            </a>
                                        </div>
                                        <div class="product-title text-center">
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                               <?php the_title(); ?>
                                            </a>
                                        </div>
                                        <div class="product-price text-center">
                                            <?php
                                                $display_price         = $product->get_display_price();
                                                if ( $product->get_price() > 0 ) {
                                                    echo number_format($display_price, 0, ",", '.' );
                                                }
                                                $feat_image = wp_get_attachment_url( get_post_thumbnail_id($product->ID) );
                                             ?>
                                        </div>
                                        <div class="product-buttons text-center">
                                            <a href="<?php echo $product->add_to_cart_url(); ?>" data-quantity="1" data-product_id="<?php echo $product->id; ?>" data-product_sku="<?php echo $product->get_sku(); ?>" class="add-to-cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                            <a href="<?php echo $product->add_to_cart_url(); ?>" class="btn btn-primary quick-buy" data-id="<?php echo $product->id; ?>" data-title="<?php the_title(); ?>" data-price="<?php echo $product->get_display_price(); ?>" data-image="<?php echo $feat_image; ?>"><?php _e( 'Mua ngay', __THEMEDOMAIN__ ); ?></a>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            <?php wp_reset_postdata(); ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div><!-- END .container -->
        <?php
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title = ! empty( $instance['title'] ) ? $instance['title'] : __( 'Máy vi tính' );
        $product_cat = ! empty( $instance['product_cat'] ) ? $instance['product_cat'] : 0;
        $color = ! empty( $instance['color'] ) ? $instance['color'] : '#70ba28';
        $icon = ! empty( $instance['icon'] ) ? $instance['icon'] : 0;
        $align = ! empty( $instance['align'] ) ? $instance['align'] : 'left';
        $link = ! empty( $instance['link'] ) ? $instance['link'] : '';
        $img_ads = ! empty( $instance['img_ads'] ) ? $instance['img_ads'] : 0;
        wp_enqueue_script('jquery');
        wp_enqueue_media();
        wp_enqueue_script('media-upload');
        wp_enqueue_script('thickbox');
        $thumbnail_id = $icon;
        if ( $thumbnail_id ) {
            list($image, $width, $height) = wp_get_attachment_image_src($thumbnail_id, 'full');
        } else {
            $image = get_template_directory_uri().'/assets/images/home-may-vi-tinh-icon.png';
        }

        if ( $img_ads ) {
            list($image_ads, $width, $height) = wp_get_attachment_image_src($img_ads, 'full');
        } else {
            $image_ads = get_template_directory_uri().'/assets/images/may-vi-tinh.jpg';
        }

        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'product_cat' ); ?>"><?php _e( 'Danh mục sản phẩm:' ); ?></label>
            <?php
                $args = array(
                    'show_option_all'    => 'Chọn danh mục sản phẩm',
                    'show_option_none'   => '',
                    'option_none_value'  => '-1',
                    'orderby'            => 'ID',
                    'order'              => 'ASC',
                    'show_count'         => 0,
                    'hide_empty'         => 0,
                    'child_of'           => 0,
                    'exclude'            => '',
                    'echo'               => 1,
                    'selected'           => $product_cat,
                    'hierarchical'       => 1,
                    'name'               => $this->get_field_name( 'product_cat' ),
                    'id'                 => $this->get_field_id( 'product_cat' ),
                    'class'              => 'postform',
                    'depth'              => 0,
                    'tab_index'          => 0,
                    'taxonomy'           => 'product_cat',
                    'hide_if_empty'      => false,
                    'value_field'	     => 'term_id',
                );
                wp_dropdown_categories( $args );
            ?>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'color' ); ?>"><?php _e( 'Màu biểu tượng:' ); ?></label>
            <input type="text" id="<?php echo $this->get_field_id( 'color' ); ?>" name="<?php echo $this->get_field_name( 'color' ); ?>" value="<?php echo esc_attr( $color ); ?>" class="color-picker widefat" data-default-color="#70ba28" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'icon' ); ?>"><?php _e( 'Hình biểu tượng:' ); ?></label>
            <button type="button" id="<?php echo $this->get_field_id( 'upload-image' ); ?>" class="button upload_img" data-id="<?php echo $this->get_field_id( 'icon' ); ?>" data-image="<?php echo $this->get_field_id( 'field-image' ); ?>"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
            <div id="<?php echo $this->get_field_id( 'field-image' ); ?>" class="field-image">
                <img src="<?php echo $image; ?>" alt="Hình biểu tượng" />
            </div>
            <input type="hidden" id="<?php echo $this->get_field_id( 'icon' ); ?>" name="<?php echo $this->get_field_name( 'icon' ); ?>" value="<?php echo esc_attr( $icon ); ?>" class="icon" />
        </p>
        <style type="text/css">
        .field-image {
            background: gray;
            width: 100%;
            margin: 0 auto;
            padding: 20px;
            text-align: center;
            box-sizing: border-box;
        }
        .field-image img{
            max-width: 100%;
        }
        </style>
        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : __( 'Máy vi tính' );
        $instance['product_cat'] = ( ! empty( $new_instance['product_cat'] ) ) ? strip_tags( $new_instance['product_cat'] ) : 0;
        $instance['color'] = ( ! empty( $new_instance['color'] ) ) ? strip_tags( $new_instance['color'] ) : '#70ba28';
        $instance['icon'] = ( ! empty( $new_instance['icon'] ) ) ? strip_tags( $new_instance['icon'] ) : 0;
        $instance['align'] = ( ! empty( $new_instance['align'] ) ) ? strip_tags( $new_instance['align'] ) : 'left';
        $instance['link'] = ( ! empty( $new_instance['link'] ) ) ? strip_tags( $new_instance['link'] ) : '';
        $instance['img_ads'] = ( ! empty( $new_instance['img_ads'] ) ) ? strip_tags( $new_instance['img_ads'] ) : 0;

        return $instance;
    }

}