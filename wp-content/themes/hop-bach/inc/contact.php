<?php
/*
 * Template Name: Contact
 */
?>
<?php get_header();?>
    <div class="container-flued">
        <?php
        while(have_posts()):the_post();
            echo get_post_meta( get_the_ID(), '_page_top_setting_content', true );
        endwhile;
        ?>
    </div>
    <div class="container">
        <div class="row km_row1">
            <div class="col-md-12 product-header"><?php woocommerce_breadcrumb(array('delimiter'=>'&nbsp;&gt;&nbsp;')); ?></div>
        </div>
    </div>
        <div class="maps">
            <?php echo do_shortcode('[wpgmza id="1"]');?>
        </div>
    <div class="container">
        <div class="row contact-detail">
            <?php if (have_posts()) while (have_posts()) the_post();
                the_content();
            ?>
        </div>
    </div>
<?php get_footer();?>