<?php
/*
 * Template Name: Khuyen Mai
 */
?>
<?php get_header();?>
    <div class="container-flued">
        <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-banner.png" alt="banner"/>
    </div>
    <div class="container">
        <div class="row km_row1">
            <div class="col-md-12 product-header"><?php woocommerce_breadcrumb(array('delimiter'=>'&nbsp;&gt;&nbsp;')); ?></div>
        </div>
        <div class="row km_row2">
            <div class="sidebar col-lg-3 col-sm-12">
                <div class="sale">
                </div><!-- end .salte-->

                <div class="support">
                    <h3 class="text-uppercase">hỗ trợ trực tuyến</h3>
                    <div class="inner">
                        <h4 class="text-uppercase title banhang">bán hàng</h4>
                        <div class="row sp1">
                            <div class="col-lg-4">
                                <img  class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-hotro1.png" alt="hotro1"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase">ms trang </h4>
                                <p class="phone">Điện thoaị:<span class="color-red">123456789</span></p>
                                <p class="mobile">Di động:<span class="color-red">123456789</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-hotro2.png" alt="hotro2"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase">ms xuan</h4>
                                <p class="phone">Điện thoaị:<span class="color-red">123456789</span></p>
                                <p class="mobile">Di động:<span class="color-red">123456789</span></p>
                            </div>
                        </div>

                        <h4 class="text-uppercase title kythuat">Kỹ thuật</h4>
                        <div class="row kythuat">
                            <div class="col-lg-4">
                                <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-hotro3.png" alt="hotro3"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase">mss thanh</h4>
                                <p class="phone">Điện thoaị:<span class="color-red">123456789</span></p>
                                <p class="mobile">Di động:<span class="color-red">123456789</span></p>
                            </div>
                        </div>
                    </div>
                </div><!-- end .support-->

                <div class="get-sale">
                    <h3 class="text-uppercase">nhận tin khuyến mãi</h3>
                    <div class="inner">
                        <p>Hãy nhập Email của bạn và nhấn nút "Đăng ký", để nhận được các thông tin mới nhất của <strong>HỢP BÁCH</strong> qua email.</p>
                        <?php if ( is_active_sidebar( 'email-nhan-tin' ) ) : ?>
                                <?php dynamic_sidebar( 'email-nhan-tin' ); ?>
                        <?php endif; ?>
                    </div>
                </div><!-- end .get-sale-->
            </div>
            <div class="col-lg-9 col-sm-12">
                <div class="km_content">
                    <div class="row">
                        <div class="col-lg-4">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-khuyenmai1.png" alt=""/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="title text-uppercase">thông tin khuyến mãi 1</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <a class="read-more" href="">Đọc thêm <span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-khuyenmai2.png" alt=""/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="title text-uppercase">thông tin khuyến mãi 2</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-khuyenmai3.png" alt=""/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="title text-uppercase">thông tin khuyến mãi 3</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-khuyenmai4.png" alt=""/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="title text-uppercase">thông tin khuyến mãi 4</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-khuyenmai5.png" alt=""/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="title text-uppercase">thông tin khuyến mãi 5</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-chevron-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php get_footer();?>