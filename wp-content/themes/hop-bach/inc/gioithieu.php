<?php
/*
 * Template Name: Gioi Thieu
 */
?>

<?php get_header(); ?>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.gioithieu p:not(:first)').hide();
            $('h5').click(function(){
                $('h5').removeClass('current');
                $(this).addClass('current');
                $('.gioithieu p:visible').stop().slideUp(500);
                $(this).next().stop().slideDown(500);
            });
        })
    </script>
<div class="banner">
    <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/gt-banner.png" alt=""/>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 product-header"><?php woocommerce_breadcrumb(array('delimiter'=>'&nbsp;&gt;&nbsp;')); ?></div>
    </div>
</div>
<?php
if (have_posts()) while (have_posts()) the_post();
the_content();
?>

<div class="container-fluid nhantin">
    <div class="container">
        <div class="row">
            <div class="text text-center">
               <h3 class="text-uppercase "><strong>nhận tin khuyến mãi</strong></h3>
                <p><?php echo ot_get_option('thay_doi_text'); ?></p>
                <div class="col-lg-6 col-lg-offset-3">
                    <?php if ( is_active_sidebar( 'email-khach-hang' ) ) : ?>
                        <ul id="sidebar">
                            <?php dynamic_sidebar( 'email-khach-hang' ); ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="footerwhite">
    <div class="container footer-info">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img class="img-responsive" src="<?php echo ot_get_option('anh_hang_dep'); ?>" alt=""/>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <h4 class="text-uppercase"><strong><?php echo ot_get_option('hang_dep'); ?></strong></h4>
                        <p><?php echo ot_get_option('mo_ta_hang_dep'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img class="img-responsive" src="<?php echo ot_get_option('anh_chat_luong'); ?>" alt=""/>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <h4 class="text-uppercase"><strong><?php echo ot_get_option('chat_luong'); ?></strong></h4>
                        <p><?php echo ot_get_option('mo_ta_chat_luong'); ?></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3">
                        <img class="img-responsive" src="<?php echo ot_get_option('anh_gia_ca'); ?>" alt=""/>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-9">
                        <h4 class="text-uppercase"><strong> <?php echo ot_get_option('gia_ca'); ?></strong></h4>
                        <p><?php echo ot_get_option('mo_ta_gia_ca'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php get_footer(); ?>