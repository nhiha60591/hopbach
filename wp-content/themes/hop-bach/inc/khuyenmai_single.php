<?php
/*
 * Template Name: single khuyen mai
 */
?>
<?php get_header();?>
    <div class="container-flued">
        <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/contact-banner.png" alt="banner"/>
    </div>
    <div class="container">
        <div class="row km_row1">
            <div class="col-md-12 product-header"><?php woocommerce_breadcrumb(array('delimiter'=>'&nbsp;&gt;&nbsp;')); ?></div>
        </div>
        <div class="row km_row2 kmsg_row2">
            <div class="sidebar col-lg-3 col-sm-12">
                <div class="sale">
                </div><!-- end .salte-->

                <div class="support">
                    <h3 class="text-uppercase">hỗ trợ trực tuyến</h3>
                    <div class="inner">
                        <h4 class="text-uppercase title banhang">bán hàng</h4>
                        <div class="row sp1">
                            <div class="col-lg-4">
                                <img  class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-hotro1.png" alt="hotro1"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase">ms trang </h4>
                                <p class="phone">Điện thoaị:<span class="color-red">123456789</span></p>
                                <p class="mobile">Di động:<span class="color-red">123456789</span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-hotro2.png" alt="hotro2"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase">ms xuan</h4>
                                <p class="phone">Điện thoaị:<span class="color-red">123456789</span></p>
                                <p class="mobile">Di động:<span class="color-red">123456789</span></p>
                            </div>
                        </div>

                        <h4 class="text-uppercase title kythuat">Kỹ thuật</h4>
                        <div class="row kythuat">
                            <div class="col-lg-4">
                                <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-hotro3.png" alt="hotro3"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase">mss thanh</h4>
                                <p class="phone">Điện thoaị:<span class="color-red">123456789</span></p>
                                <p class="mobile">Di động:<span class="color-red">123456789</span></p>
                            </div>
                        </div>
                    </div>
                </div><!-- end .support-->

                <div class="get-sale">
                    <h3 class="text-uppercase">nhận tin khuyến mãi</h3>
                    <div class="inner">
                        <p>Hãy nhập Email của bạn và nhấn nút "Đăng ký", để nhận được các thông tin mới nhất của <strong>HỢP BÁCH</strong> qua email.</p>
                        <?php if ( is_active_sidebar( 'email-nhan-tin' ) ) : ?>
                            <?php dynamic_sidebar( 'email-nhan-tin' ); ?>
                        <?php endif; ?>
                    </div>
                </div><!-- end .get-sale-->
            </div>
            <div class="col-lg-9 col-sm-12">
                <div class="kmsg_conten">
                    <div class="row">
                        <div class="col-lg-12">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri();?>/assets/images/km-images.png" alt=""/>
                            <div class="row">
                                <div class="col-lg-6 col-sm-5 col-xs-6 left">
                                    <p class="date"><span class="big">23 /</span>thang 11 2016</p>
                                </div>
                                <div class="col-lg-6 col-sm-7 col-xs-6 text-right" >
                                    <p class="comment ">02 binh luan</p>
                                    <p class="author ">Liem Nguyen</p>
                                </div>

                            </div>
                        </div>
                        <div class=" col-lg-12">
                            <div class="detail">
                                <h4 class="title">THÔNG TIN KHUYẾN MÃI 1</h4>
                                    <p>Dịch vụ Bảo Hành Tận Nhà của HP Việt Nam đã chính thức được triển khai sau một thời gian thử nghiệm và được khách hàng đón nhận cũng như đánh giá cao. Đồng thời, dịch vụ Tổng Đài Hỗ Trợ Khách Hàng của hãng cũng được nâng cấp lên 24/7 – hỗ trợ khách hàng 24 giờ trong một ngày và 7 ngày trong một tuần bao gồm cả ngày lễ, nhằm tăng tính đáp ứng hỗ trợ bất cứ khi nào khách hàng cần. Đây đều là những dịch vụ rất thiết thực và hiệu quả dành cho những khách hàng trung thành của HP.</p>
                                    <p>Cùng với hai dịch vụ đặc biệt trên kết hợp với các dịch vụ khác như Bảo Hành VIP 30 Phút, Bảo Hành Giao & Nhận Tại Nhà Miễn Phí, HP Việt Nam đang nỗ lực đảm bảo sự tin tưởng và tín nhiệm của khách hàng ở mức cao nhất đối với thương hiệu HP.</p>
                                    <p>Nhằm tăng cường độ phủ dịch vụ, HP Việt Nam cũng đã khai trương thêm một trung tâm bảo hành mới tại Cần Thơ trong tháng 10-2015, nâng tổng số lên bốn Trung Tâm Chăm Sóc Khách Hàng Toàn Diện HP trên toàn quốc. Với đầy đủ các dịch vụ hỗ trợ khách hàng thống nhất tại cả bốn trung tâm trải dài từ Hà Nội, Đà Nẵng đến Thành phố Hồ Chí Minh, Cần Thơ, giờ đây HP Việt Nam đã có thể cung cấp mạng lưới hỗ trợ dịch vụ toàn diện nhất cho khách hàng của mình.</p
                                    <p>Các dịch vụ hỗ trợ khách hàng hiện nay của HP Việt Nam gồm có:</p>
                                <h6>Bảo Hành Tận Nhà</h6>
                                    <p>Khi sản phẩm gặp sự cố, khách hàng có thể liên hệ với tổng đài hỗ trợ khách hàng của HP để kiểm tra và xác định lỗi từ xa. Tổng đài sau đó sẽ xếp lịch hẹn và cử kỹ thuật viên đến thực hiện việc bảo hành, thay thế linh kiện cho sản phẩm tận nhà trong vòng 2 ngày làm việc nếu khách hàng sinh sống tại các thành phố có các trung tâm chăm sóc khách hàng toàn diện HP, bao gồm Hà Nội, Đà Nẵng, TP. Hồ Chí Minh và Cần Thơ, và trong vòng 3 ngày nếu khách hàng sinh sống ở khu vực khác. Dịch vụ bảo hành sửa chữa máy tận nhà sẽ được áp dụng từ ngày 1 tháng 10 năm 2015.</p>
                                    <p>Tổng Đài Hỗ Trợ Khách Hàng 24/7 (1800 58 88 68)</p>
                                    <p> Tổng Đài Hỗ Trợ Khách Hàng 24/7 là dịch vụ hỗ trợ khách hàng mới của HP vừa đi vào hoạt động từ ngày 15/9/2015 nhằm mang lại sự thuận tiện cho khách hàng cũng như giúp nâng cao chất lượng dịch vụ của HP Việt Nam. Khách hàng khi gặp sự cố hoặc có bất cứ thắc mắc nào về sản phẩm HP đều có thể gọi điện thoại tới số hỗ trợ dịch vụ 1800 58 88 68 (miễn phí) để được hỗ trợ tận tình, chu đáo. Để biết thêm thông tin về dịch vụ Tổng Đài Hỗ Trợ Khách Hàng 24/7, xin vui lòng gọi số 1800 58 88 68 (miễn phí) hoặc địa chỉ e-mai: cssvn@hp.com.</p>
                                <h6>Bảo Hành VIP 30 Phút</h6>
                                    <p>Khách hàng có thể mang sản phẩm gặp các lỗi cơ bản như ổ cứng (HDD), ổ quang (ODD), bàn phím (Keyboard), pin (Battery), bộ sạc (Adapter), RAM hay linh kiện không dây (Wireless) đến kiểm tra và xác định lỗi tại Trung Tâm Chăm Sóc Khách Hàng Toàn Diện HP. Bộ phận tiếp nhận sau đó sẽ thực hiện việc bảo hành, thay thế linh kiện trong vòng 30 phút. Đối với những lỗi phức tạp hơn chưa thể xử lý ngay, sản phẩm sẽ được áp dụng theo quy trình bảo hành bình thường tại Trung Tâm Chăm Sóc Khách Hàng Toàn Diện HP.</p>
                                <h6>Bảo Hành Giao & Nhận Tại Nhà Miễn Phí</h6>
                                    <p>Khách hàng gặp sự cố với sản phẩm có thể liên lạc đến Tổng Đài Hỗ Trợ Khách Hàng 24/7 (1800 58 88 68) của HP để kiểm tra và xác định lỗi từ xa. Nếu lỗi sản phẩm không thể được khắc phục qua điện thoại, HP sẽ cử nhân viên giao nhận từ Viettelpost đến tiếp nhận sản phẩm và gửi về Trung Tâm Chăm Sóc Khách Hàng Toàn Diện HP để sửa chữa. Sản phẩm hoàn thiện sẽ được giao lại cho khách hàng cũng thông qua dịch vụ giao nhận của Viettelposts. Dịch vụ Bảo Hành Giao & Nhận Tại Nhà Miễn Phí được triển khai tại 63 tỉnh thành trên toàn quốc.</p>
                                <h6>Trung Tâm Chăm Sóc Khách Hàng Toàn Diện HP</h6>
                                    <p>Hiện HP Việt Nam đang có 4 trung tâm chăm sóc khách hàng đặt tại Hà Nội, Thành phố Hồ Chí Minh, Đà Nẵng và Cần Thơ.</p>
                                    <ul>
                                        <li>Hà Nội: Phòng 102, Tháp B, Tầng 1, Tòa nhà Resco Tower, 521 Kim Mã, Quận Ba Đình. Điện thoại: (+84)4 3935 0565</li>
                                        <li>TP. Hồ Chí Minh: 37 Tú Xương, Phường 7, Quận 3. Điện thoại: (+84)8 3932 4040</li>
                                        <li>Đà Nẵng: 60 Nguyễn Du, Quận Hải Châu. Điện thoại: (+84)511 38 222 38</li>
                                        <li>Cần Thơ: 129 Lý Tự Trọng, Quận Ninh Kiều. Điện thoại: (+84)710 3734 666</li>
                                    </ul>
                                    <p>Tổng Đài Hỗ Trợ Khách Hàng 24/7: 1800 58 88 68</p>
                                    <p>Email: cssvn@hp.com</p>
                            </div>
                        </div>
                        <div class="share col-lg-12">
                            <div class="tag">
                                <p><strong>Tag: </strong>HP, Bao Hanh, Tong dai, Su Co</p>
                            </div>
                        </div>
                    </div><!-- end .row-->
                </div><!-- end .kmsg_conten-->
            </div><!-- end .col-lg-9-->
        </div>
    </div>
<?php get_footer();?>