<?php
/**
 * Create all functions for Hop Bach Theme
 * @author Hien(Hamilton) H.HO
 * @date 11-May-2016
 * @email nhiha60591@gmail.com
 */
if (!isset($content_width)) {
    $content_width = 330;
}
if ( ! class_exists( 'HopBach_Theme' ) ) :
    /**
     * Hop Bac Theme Class
     * @creator Hien
     */
    class HopBach_Theme
    {
        /**
         * The single instance of the class.
         *
         * @var HopBach
         * @since 2.1
         */
        protected static $_instance = null;
        /**
         * Main HopBach Instance.
         *
         * Ensures only one instance of HopBach is loaded or can be loaded.
         *
         * @since 2.1
         * @static
         * @see HBT()
         * @return HopBach - Main instance.
         */
        public static function instance() {
            if ( is_null( self::$_instance ) ) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * HopBach Constructor.
         */

        public function __construct() {
            //add_filter ('add_to_cart_redirect', array( $this ,'redirect_to_checkout'));
            add_action( 'init', array( $this, 'create_post_type' ) );
            add_action( 'add_meta_boxes', array( __CLASS__, 'post_meta_boxes' ) );
            add_action( 'save_post', array( __CLASS__, 'save_post_data' ) );
            $this->define_constants();
            $this->includes();
            $this->init_hooks();

            do_action( 'hopbach_loaded' );
        }

        function redirect_to_checkout() {
            global $woocommerce;
            $checkout_url = $woocommerce->cart->get_checkout_url();
            return $checkout_url;
        }
        /**
         * Hook into actions and filters.
         * @since  2.3
         */
        private function init_hooks() {
            add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
            add_action( 'wp_enqueue_scripts', array( $this, 'theme_scripts' ) );
            add_action( 'widgets_init', array( $this, 'widgets_init' ) );
            add_action( 'admin_enqueue_scripts', array( $this, 'admin_scripts' ) );
            add_action( 'init', array( $this, 'init' ), 1 );
            add_action( 'wp_footer', array( $this, 'footer_popup' ) );
            add_action( 'pre_get_posts', array( $this, 'product_filter' ) );
        }
        public function init(){
            remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20, 0 );
            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30, 0 );
            remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10, 0 );
            remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10, 0 );
            remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10, 0 );
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10, 0 );
            add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );
            add_action( 'woocommerce_single_product_summary', array( $this, 'share_product' ), 20 );

            add_action( 'wp_ajax_hb_qb', array( $this, 'ajax_quick_buy' ) );
            add_action( 'wp_ajax_nopriv_hb_qb', array( $this, 'ajax_quick_buy' ) );
            if ( isset( $_GET['clear-cart'] ) ) {
                global $woocommerce;
                $woocommerce->cart->empty_cart();
            }
        }

        /**
         * Define HBT Constants.
         */
        private function define_constants() {
            define( '__THEMEDOMAIN__', 'hopbach' );
        }

        /**
         * Include required core files used in admin and on the frontend.
         */
        public function includes() {
            if( !is_admin() ){
                $this->frontend_includes();
            }
            include "includes/widgets/class-home-product-widget.php";
            include "includes/widgets/class-product-sale-widget.php";
        }

        /**
         * Include required frontend files.
         */
        public function frontend_includes() {
            
        }

        public function theme_setup(){
            /*
             * Make theme available for translation.
             * Translations can be filed in the /languages/ directory.             
             */
            load_theme_textdomain( __THEMEDOMAIN__, get_template_directory() . '/languages' );

            // Add default posts and comments RSS feed links to head.
            add_theme_support( 'automatic-feed-links' );

            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support( 'title-tag' );


            /*
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
             */
            add_theme_support( 'post-thumbnails' );
            set_post_thumbnail_size( 1200, 9999 );

            // This theme uses wp_nav_menu() in two locations.
            register_nav_menus( array(
                'header' => __( 'Header Menu', __THEMEDOMAIN__ ),
                'primary'  => __( 'Primary Menu', __THEMEDOMAIN__ ),
                'danhmucsanpham'  => __( 'Danh mục sản phẩm ', __THEMEDOMAIN__ ),
            ) );

            /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            ) );

            /*
             * Enable support for Post Formats.
             *
             * See: https://codex.wordpress.org/Post_Formats
             */
            add_theme_support( 'post-formats', array(
                'aside',
                'image',
                'video',
                'quote',
                'link',
                'gallery',
                'status',
                'audio',
                'chat',
            ) );

            // Add woocommerce for theme support
            add_theme_support( 'woocommerce' );

        }

        /**
         * meta box
         */
        function post_meta_boxes(){
            add_meta_box(
                'opticab-top-setting',
                __( 'Tùy chỉnh trang', __THEMEDOMAIN__ ),
                array( __CLASS__, 'top_page_setting' ),
                'page'
            );
            add_meta_box(
                'opticab-top-setting-single',
                __( 'Add ảnh vào trong single', __THEMEDOMAIN__ ),
                array( __CLASS__, 'top_page_setting_single' ),
                'khuyen_mai'
            );
        }
        /**
         * Save post data
         */
        function save_post_data( $post_id ){
            update_post_meta( $post_id, '_page_top_setting_content', $_POST['_page_top_setting_content'] );
            update_post_meta( $post_id, '_khuyen_mai_top_setting_content', $_POST['_khuyen_mai_top_setting_content'] );
        }
        /**
         * page setting
         */
        function top_page_setting($post){
            // Add a nonce field so we can check for it later.
            wp_nonce_field( __THEMEDOMAIN__, 'page_top_setting' );
            $value = get_post_meta( $post->ID, '_page_top_setting_content', true );
            wp_editor( $value, '_page_top_setting_content', $settings = array() );
        }
        function top_page_setting_single($post){
            // Add a nonce field so we can check for it later.
            wp_nonce_field( __THEMEDOMAIN__, 'top_page_setting_single' );
            $value1 = get_post_meta( $post->ID, '_khuyen_mai_top_setting_content', true );
            wp_editor( $value1, '_khuyen_mai_top_setting_content', $settings = array() );
        }




        /**
         *Create post type
         */
        function create_post_type()
        {
            $labels = array(
                'name' => _x('Khuyến Mãi', 'post type general name', '__THEMEDOMAIN__'),
                'singular_name' => _x('Khuyến Mãi', 'post type singular name', '__THEMEDOMAIN__'),
                'menu_name' => _x('Khuyến Mãi', 'admin menu', '__THEMEDOMAIN__'),
                'name_admin_bar' => _x('Khuyến Mãi', 'add new on admin bar', '__THEMEDOMAIN__'),
                'add_new' => _x('Thêm mới', 'book', '__THEMEDOMAIN__'),
                'add_new_item' => __('Thêm mới dịch vụ', '__THEMEDOMAIN__'),
                'new_item' => __('Thêm dịch vụ', '__THEMEDOMAIN__'),
                'edit_item' => __('Sửa dịch vụ', '__THEMEDOMAIN__'),
                'view_item' => __('Xem dịch vụ', '__THEMEDOMAIN__'),
                'all_items' => __('Tất cả dịch vụ', '__THEMEDOMAIN__'),
                'search_items' => __('Tìm kiếm dịch vụ', '__THEMEDOMAIN__'),
                'parent_item_colon' => __('Dịch vụ hiện tại:', '__THEMEDOMAIN__'),
                'not_found' => __('Không tìm thấy dịch vụ.', '__THEMEDOMAIN__'),
                'not_found_in_trash' => __('Không tìm thấy dịch vụ trong thùng rác.', '__THEMEDOMAIN__')
            );

            $args = array(
                'labels' => $labels,
                'description' => __('Description.', '__THEMEDOMAIN__'),
                'public' => true,
                'publicly_queryable' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'query_var' => true,
                'rewrite' => array('slug' => 'khuyen-mai'),
                'capability_type' => 'post',
                'has_archive' => true,
                'hierarchical' => false,
                'menu_position' => null,
                'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','tags'),
                'taxonomies' => array('post_tag'),
                'register_meta_box_cb' => 'opticab-top-setting-silge'

            );

            register_post_type('khuyen_mai', $args);
        }
        /**
         * All theme scripts
         * @author Hien
         */
        public function theme_scripts(){
            // Style
            wp_enqueue_style('bootstrap-style', get_template_directory_uri() . "/assets/css/bootstrap.min.css");
            wp_enqueue_style('font-awesome-style', get_template_directory_uri() . "/assets/css/font-awesome.min.css");
            wp_enqueue_style('style', get_stylesheet_uri());
            wp_enqueue_style('style-responsive', get_template_directory_uri()."/responsive.css" );

            // Scripts
            wp_enqueue_script('bootstrap', get_template_directory_uri(). "/assets/js/bootstrap.js",array('jquery'));
            wp_enqueue_script('gt_cacdichvu', get_template_directory_uri(). "/assets/js/gt_cacdichvu.js",array('jquery'));
            wp_enqueue_script('validate', get_template_directory_uri(). "/assets/js/jquery.validate.min.js",array('jquery'));
            wp_enqueue_script('huuhien', get_template_directory_uri(). "/assets/js/huuhien.js",array('jquery'));
            wp_localize_script( 'huuhien', 'ajax_object',
                array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
        }

        /**
         * Register all widgets for theme
         */
        public function widgets_init(){
            register_sidebar( array(
                'name' => __( 'Text Nhận Tin khuyến mãi', __THEMEDOMAIN__ ),
                'id' => 'nhan-tin-khuyen-mai',
                'description' => __( 'Thêm hoặc thay đổi text ở mục nhận tin khuyến mãi qua email', __THEMEDOMAIN__ ),
                'before_widget' => '<div id="%1$s" class="widget col-md-3 %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title text-uppercase">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Khu vực cuối trang', __THEMEDOMAIN__ ),
                'id' => 'footer-area',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở dưới trang', __THEMEDOMAIN__ ),
                'before_widget' => '<div id="%1$s" class="widget col-md-3 %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title text-uppercase">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Email Khách Hàng', __THEMEDOMAIN__ ),
                'id' => 'email-khach-hang',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở dưới trang', __THEMEDOMAIN__ ),
                'before_widget' => '<div id="%1$s" class="widget  %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Email Nhận tin trang tuyến mãi', __THEMEDOMAIN__ ),
                'id' => 'email-nhan-tin',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở dưới trang', __THEMEDOMAIN__ ),
                'before_widget' => '<div id="%1$s" class="widget  %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Home Products', __THEMEDOMAIN__ ),
                'id' => 'home-product-area',
                'description' => __( 'Hiện sản phẩm ở trang chủ', __THEMEDOMAIN__ ),
                'before_widget' => '<section id="%1$s" class="home-product-widget %2$s">',
                'after_widget'  => '</section><!-- END .home-product-widget -->',
                'before_title'  => '<h2 class="widget-title text-uppercase">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Product Sidebar', __THEMEDOMAIN__ ),
                'id' => 'product-sidebar',
                'description' => __( 'Hiện sản phẩm ở sidebar product', __THEMEDOMAIN__ ),
                'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
                'after_widget'  => '</div><!-- END .div -->',
                'before_title'  => '<h2 class="sidebar-widget-title text-uppercase">',
                'after_title'   => '</h2>',
            ) );
            register_widget( 'HomeProductWidget' );
            register_widget( 'SaleProductWidget' );
        }
        public function admin_scripts(){
            // first check that $hook_suffix is appropriate for your admin page
            wp_enqueue_style( 'wp-color-picker' );
            wp_enqueue_script( 'custom-script-handle', get_template_directory_uri()."/assets/js/admin.js", array(  ), false, true );
        }

        /**
         * Show footer popup
         */
        public function footer_popup(){
            ?>
            <div class="overlay"></div>
            <div class="quick-buy-popup">
                <a class="popup-close" href="#"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/popup-close-btn.png" alt="Close"></a>
                <div class="popup-header text-center text-uppercase">
                    <h3><?php _e( 'ĐẶT HÀNG NHANH - GIAO HÀNG NGAY - NHẬN TẬN TAY', __THEMEDOMAIN__ ); ?></h3>
                </div>
                <div class="popup-body">
                    <?php
                    if( isset($_POST['muanhanh']) ){
                        $fullName = $_POST['full_name'];
                        $phone_number = $_POST['phone_number'];
                        $address = $_POST['address'];
                        $number = $_POST['number'];
                        $content = $_POST['content'];
                        $product_id = $_POST['product_id'];
                        $product = new WC_Product($product_id);
                        $body = 'Chào bạn, Có một khách hàng muốn đặt hàng một sản phẩm ở trang web của bạn \n';
                        $body .= 'Họ và tên: '.$fullName. '\n';
                        $body .= 'Số điện thoại: '.$phone_number. '\n';
                        $body .= 'Số lượng: '.$number. '\n';
                        $body .= 'Địa chỉ: '.$address. '\n';
                        $body .= 'Nội dung: '.$content. '\n';
                        $body .= 'Thông tin sản phẩm: \n';
                        $body .= "#{$product_id} - {$product->get_title()} \n";
                        $adminMail = get_option( 'admin_email', 'nhiha60591@gmail.com' );
                        wp_mail( $adminMail, 'Có một khách hàng liên hệ mua nhanh', $body );
                    }
                    ?>
                    <form name="" action="" method="post" id="popup-form-quick-buy">
                        <input type="hidden" name="product_id" value="0" />
                        <div class="col-md-4">
                            <div class="popup-pr-image text-center">
                                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/may-vi-tinh.jpg" alt="May vi tinh" />
                            </div>
                            <div class="popup-pr-title text-center">
                                CASE JETEK
                            </div>
                            <div class="popup-pr-price">
                                <div class="product-price text-center"><span class="amount">123,123,123&nbsp;₫</span></div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <p><input type="text" class="input-text" required name="full_name" placeholder="<?php _e('Tên của bạn (*)'); ?>" /></p>
                            <p><input type="text" class="input-text" required name="phone_number" placeholder="<?php _e('Số điện thoại (*)'); ?>" /></p>
                            <p><input type="text" class="input-text" required name="address" placeholder="<?php _e('Địa chỉ (*)'); ?>" /></p>
                            <p>Số lượng: <input type="number" value="1" name="number" /></p>
                            <p><textarea name="content" class="textarea-wrap" required></textarea></p>
                            <p class="text-right"><input type="submit" name="muanhanh" class="btn btn-red" value="<?php _e( 'Gửi mail', __THEMEDOMAIN__ ); ?>"></p>
                        </div>
                    </form>
                </div>
            </div>
            <?php
        }
        public function share_product(){
            ?>
            <div class="row product-share">
                <div class="col-md-6">
                    Share
                </div>
                <div class="col-md-6">
                    <?php echo do_shortcode('[supsystic-social-sharing id="2"]') ?>
                </div>
            </div>
            <?php
        }
        public function product_filter($query){
            if ( isset( $_GET['posts_per_page'] ) ) {
                $query->set( 'posts_per_page', $_GET['posts_per_page'] );
            }
            if ( isset( $_GET['hbprc'] ) && $_GET['hbprc'] > 0 ) {
                $tax_query = array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field'    => 'term_id',
                        'terms'    => (array)$_GET['hbprc'],
                        'operator' => 'IN'
                    ),
                );
                $query->tax_query = $tax_query;
            }
        }
        public function ajax_quick_buy(){
            $product_id = $_POST['product_id'];
            global $woocommerce;
            $woocommerce->cart->empty_cart();
            $woocommerce->cart->add_to_cart($product_id);
            echo $woocommerce->cart->get_checkout_url();
            die();
        }
    }
endif;

/**
 * Main instance of HopBach.
 *
 * Returns the main instance of WC to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return HopBach
 */
function HBT() {
    return HopBach_Theme::instance();
}

// Global for backwards compatibility.
$GLOBALS['hopbach'] = HBT();