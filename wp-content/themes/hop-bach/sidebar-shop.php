<aside class="col-md-3  sidebar" id="sidebar">
    <div class="sidebar-widget km_row2 sidebar-shop">
        <div class="sidebar">
            <div class="sale">
                <h3 class="text-uppercase">SALE</h3>
                <?php
                $args = array(
                    'post_type'      => 'product',
                    'posts_per_page' => 4,
                    'meta_query'     => array(
                        'relation' => 'OR',
                        array( // Simple products type
                            'key'           => '_sale_price',
                            'value'         => 0,
                            'compare'       => '>',
                            'type'          => 'numeric'
                        ),
                        array( // Variable products type
                            'key'           => '_min_variation_sale_price',
                            'value'         => 0,
                            'compare'       => '>',
                            'type'          => 'numeric'
                        )
                    )
                );
                $loop = new WP_Query( $args );?>
                <div class="inner">
                    <?php while ( $loop->have_posts() ) : $loop->the_post();?>

                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-xs-5">
                                <?php echo get_the_post_thumbnail();?>
                            </div>
                            <div class="col-lg-8 col-md-6 col-xs-7">
                                <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                <h5 class="price">
                                    <?php
                                    global $product;
                                    $display_price         = $product->get_display_price();
                                    if ( $product->get_price() > 0 ) {
                                        echo number_format($display_price, 0, ",", '.' );
                                    }?>
                                </h5>
                            </div>
                        </div>

                    <?php endwhile;

                    wp_reset_postdata();
                    ?>
                </div>

            </div><!-- end .salte-->

            <div class="support">
                <h3 class="text-uppercase">hỗ trợ trực tuyến</h3>
                <div class="inner">
                    <h4 class="text-uppercase title banhang">bán hàng</h4>
                    <div class="row sp1">
                        <div class="col-lg-4 col-md-4 col-xs-4">
                            <img  class="img-responsive" src="<?php echo ot_get_option('hinh_dai_dien_ho_tro_ban_hang_1'); ?>" alt="hotro1"/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="text-uppercase"><?php echo ot_get_option('ten_nguoi_ho_tro_1'); ?> </h4>
                            <p class="phone">Điện thoaị:<span class="color-red"><?php echo ot_get_option('so_dien_thoai_nguoi_ho_tro_1'); ?></span></p>
                            <p class="mobile">Di động:<span class="color-red"><?php echo ot_get_option('so_di_dong_nguoi_ho_tro_1'); ?></span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-xs-4">
                            <img class="img-responsive" src="<?php echo ot_get_option('hinh_dai_dien_ho_tro_ban_hang_2'); ?>" alt="hotro2"/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="text-uppercase"><?php echo ot_get_option('ten_nguoi_ho_tro_2'); ?></h4>
                            <p class="phone">Điện thoaị:<span class="color-red"><?php echo ot_get_option('so_dien_thoai_nguoi_ho_tro_2'); ?></span></p>
                            <p class="mobile">Di động:<span class="color-red"><?php echo ot_get_option('so_di_dong_nguoi_ho_tro_2'); ?></span></p>
                        </div>
                    </div>

                    <h4 class="text-uppercase title kythuat">Kỹ thuật</h4>
                    <div class="row kythuat">
                        <div class="col-lg-4 col-md-4 col-xs-4">
                            <img class="img-responsive" src="<?php echo ot_get_option('hinh_dai_dien_ho_tro_ban_hang_3'); ?>" alt="hotro3"/>
                        </div>
                        <div class="col-lg-8">
                            <h4 class="text-uppercase"><?php echo ot_get_option('ten_nguoi_ho_tro_3'); ?></h4>
                            <p class="phone">Điện thoaị:<span class="color-red"><?php echo ot_get_option('so_dien_thoai_nguoi_ho_tro_3'); ?></span></p>
                            <p class="mobile">Di động:<span class="color-red"><?php echo ot_get_option('so_di_dong_nguoi_ho_tro_3'); ?></span></p>
                        </div>
                    </div>
                </div>
            </div><!-- end .support-->

            <div class="get-sale">
                <h3 class="text-uppercase">nhận tin khuyến mãi</h3>
                <div class="inner">
                    <p><?php echo ot_get_option('thay_doi_text'); ?></p>
                    <?php if ( is_active_sidebar( 'email-nhan-tin' ) ) : ?>
                        <?php dynamic_sidebar( 'email-nhan-tin' ); ?>
                    <?php endif; ?>
                </div>
            </div><!-- end .get-sale-->

            <div class="banner">
                    <a target="_blank" href="<?php echo ot_get_option('link_banner_sidebar'); ?>"><div class="inner-banner"><img class="img-responsive" src="<?php echo ot_get_option('img_banner_sidebar'); ?>" alt=""/></div></a>
            </div>
        </div>
    </div>
</aside>
<aside class="col-md-4 sidebar" id="sidebar">
    <?php if ( is_active_sidebar( 'product-sidebar' ) ) : ?>
    <?php dynamic_sidebar( 'product-sidebar' ); ?>
<?php endif; ?>
</aside>
