<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Hop Bach
 * @since Hop Bach 1.0.0
 */
?>
<?php get_header(); ?>
    <section id="slider">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php echo do_shortcode('[rev_slider alias="home_slider"]'); ?>
            </div>
        </div>
    </div><!-- END .container -->
</section><!-- END .sidebar -->
<?php if ( is_active_sidebar( 'home-product-area' ) ) : ?>
    <?php dynamic_sidebar( 'home-product-area' ); ?>
<?php endif; ?>
<?php get_footer(); ?>