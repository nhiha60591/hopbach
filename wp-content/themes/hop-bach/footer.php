	<footer class="footer-widget">
		<div class="container">
			<div class="row">
				<?php if ( is_active_sidebar( 'footer-area' ) ) : ?>
					<?php dynamic_sidebar( 'footer-area' ); ?>
				<?php endif; ?>
			</div>
		</div>
	</footer>
	<footer class="footer-copy-right">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php if(function_exists('ot_get_option') ): ?>
						<p class="text-center"><?php echo ot_get_option('copy_right'); ?></p>
					<?php else: ?>
						<p class="text-center">© Copyright 2015 by <b>Hợp Bách</b>. All Rights Reserved. Trụ sở chính: 218 Nguyễn Ái Quốc, P.Trung Dũng, Biên Hòa, Đồng Nai</p>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</footer>
	<?php wp_footer(); ?>
</body>
</html>