<?php get_header();?>
    <div class="container-flued">
        <div class="top-banner">
            <?php
            global $post;
            while(have_posts()):the_post();
                echo get_post_meta( 46, '_page_top_setting_content', true );
            endwhile;
            ?>
        </div>
    </div>
    <div class="container">
        <div class="row km_row1">
            <div class="col-md-12 product-header"><?php woocommerce_breadcrumb(array('delimiter'=>'&nbsp;&gt;&nbsp;')); ?></div>
        </div>
        <div class="row km_row2 kmsg_row2">
            <div class="sidebar col-lg-3 col-md-3 col-sm-4 col-xs-12">
                <div class="sale">
                    <h3 class="text-uppercase">SALE</h3>
                    <?php
                    $args = array(
                        'post_type'      => 'product',
                        'posts_per_page' => 4,
                        'meta_query'     => array(
                            'relation' => 'OR',
                            array( // Simple products type
                                'key'           => '_sale_price',
                                'value'         => 0,
                                'compare'       => '>',
                                'type'          => 'numeric'
                            ),
                            array( // Variable products type
                                'key'           => '_min_variation_sale_price',
                                'value'         => 0,
                                'compare'       => '>',
                                'type'          => 'numeric'
                            )
                        )
                    );
                    $loop = new WP_Query( $args );?>
                    <div class="inner">
                        <?php while ( $loop->have_posts() ) : $loop->the_post();?>

                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-xs-5">
                                    <?php echo get_the_post_thumbnail();?>
                                </div>
                                <div class="col-lg-8 col-md-6 col-xs-7">
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                    <h5 class="price">
                                        <?php
                                        global $product;
                                        $display_price         = $product->get_display_price();
                                        if ( $product->get_price() > 0 ) {
                                            echo number_format($display_price, 0, ",", '.' );
                                        }?>
                                    </h5>
                                </div>
                            </div>

                        <?php endwhile;

                        wp_reset_postdata();
                        ?>
                    </div>

                </div><!-- end .salte-->

                <div class="support">
                    <h3 class="text-uppercase">hỗ trợ trực tuyến</h3>
                    <div class="inner">
                        <h4 class="text-uppercase title banhang">bán hàng</h4>
                        <div class="row sp1">
                            <div class="col-lg-4 col-md-4 col-xs-4">
                                <img  class="img-responsive" src="<?php echo ot_get_option('hinh_dai_dien_ho_tro_ban_hang_1'); ?>" alt="hotro1"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase"><?php echo ot_get_option('ten_nguoi_ho_tro_1'); ?> </h4>
                                <p class="phone">Điện thoaị:<span class="color-red"><?php echo ot_get_option('so_dien_thoai_nguoi_ho_tro_1'); ?></span></p>
                                <p class="mobile">Di động:<span class="color-red"><?php echo ot_get_option('so_di_dong_nguoi_ho_tro_1'); ?></span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-xs-4">
                                <img class="img-responsive" src="<?php echo ot_get_option('hinh_dai_dien_ho_tro_ban_hang_2'); ?>" alt="hotro2"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase"><?php echo ot_get_option('ten_nguoi_ho_tro_2'); ?></h4>
                                <p class="phone">Điện thoaị:<span class="color-red"><?php echo ot_get_option('so_dien_thoai_nguoi_ho_tro_2'); ?></span></p>
                                <p class="mobile">Di động:<span class="color-red"><?php echo ot_get_option('so_di_dong_nguoi_ho_tro_2'); ?></span></p>
                            </div>
                        </div>

                        <h4 class="text-uppercase title kythuat">Kỹ thuật</h4>
                        <div class="row kythuat">
                            <div class="col-lg-4 col-md-4 col-xs-4">
                                <img class="img-responsive" src="<?php echo ot_get_option('hinh_dai_dien_ho_tro_ban_hang_3'); ?>" alt="hotro3"/>
                            </div>
                            <div class="col-lg-8">
                                <h4 class="text-uppercase"><?php echo ot_get_option('ten_nguoi_ho_tro_3'); ?></h4>
                                <p class="phone">Điện thoaị:<span class="color-red"><?php echo ot_get_option('so_dien_thoai_nguoi_ho_tro_3'); ?></span></p>
                                <p class="mobile">Di động:<span class="color-red"><?php echo ot_get_option('so_di_dong_nguoi_ho_tro_3'); ?></span></p>
                            </div>
                        </div>
                    </div>
                </div><!-- end .support-->

                <div class="get-sale">
                    <h3 class="text-uppercase">nhận tin khuyến mãi</h3>
                    <div class="inner">
                        <p><?php echo ot_get_option('thay_doi_text'); ?></p>
                        <?php if ( is_active_sidebar( 'email-nhan-tin' ) ) : ?>
                            <?php dynamic_sidebar( 'email-nhan-tin' ); ?>
                        <?php endif; ?>
                    </div>
                </div><!-- end .get-sale-->
            </div>
            <div class="col-lg-9 col-md-9 col-sm-8 col-sm-12">
                <div class="kmsg_conten">
                    <div class="row" id="post-<?php the_ID();?>">
                        <?php $current_id = get_the_ID();?>
                        <?php
                            if(have_posts()) while(have_posts()){
                                the_post();?>
                        <div class="col-lg-12">
                            <?php
                            global $post;
                                echo get_post_meta( get_the_ID(), '_khuyen_mai_top_setting_content', true );
                            ?>
                            <div class="row hidden-xs">
                                <div class="col-lg-6 col-sm-5 col-xs-4 left">
                                    <?php
                                    global $wpdb;
                                    //$query ='SELECT * FROM $wpdb->posts';
                                    //$query ='SELECT $wpdb->post_date FORM $wpdb->posts WHERE ID={$current_id}';
                                   // $results = $wpdb->get_results($query);
                                    //var_dump($results);
                                    ?>
                                    <p class="date"><span class="big"><?php the_time('d /');?></span>Tháng <?php the_time('m Y');?></p>
                                </div>
                                <div class="col-lg-6 col-sm-7 col-xs-8 text-right" >
                                    <p class="comment "><?php comments_number( 0,01, $more ); ?>  bình luận</p>
                                    <p class="author "><?php the_author();?></p>
                                </div>

                            </div>
                        </div>
                        <div class=" col-lg-12">
                            <div class="detail">
                                <h4 class="title"><?php echo get_the_title();?></h4>
                                <?php the_content();?>
                            </div>
                        </div>
                        <div class=" col-lg-12">
                            <div class="share">
                                <div class="row">
                                    <div class="tag col-lg-6 ">
                                        <p><strong>Tag: </strong>
                                            <?php
                                            the_tags(' ');
                                            /*foreach($tags as $tag){
                                                 echo $tag->name.', ';
                                                echo"<pre>";
                                                var_dump($tag);
                                                echo"</pre>";
                                            }*/
                                            ?>

                                        </p>
                                    </div><!--end tag-->
                                    <div class="col-lg-6">
                                        <?php echo do_shortcode('[supsystic-social-sharing id="2"]') ?>
                                        <p class="share-title hidden-xs">Share sản phẩm</p>
                                    </div>
                                </div><!--end row-->
                            </div>

                        </div>
                                <?php }?>
                    </div><!-- end .row-->
                </div><!-- end .kmsg_conten-->
            </div><!-- end .col-lg-9-->
        </div>
    </div>
<?php get_footer();?>