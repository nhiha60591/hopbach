<?php

add_action('admin_enqueue_scripts','social_admin_css_styles');
add_action('wp_enqueue_scripts','social_admin_css_styles');
// add_action('load-widgets.php','social_admin_css_styles');
function social_admin_css_styles(){
	// add social style css file
	wp_register_style( 'social-icons', SOCIAL_NETWORK_URL.'assets/css/social-icons.css', false, SOCIAL_NETWORK_VERSION );
	wp_enqueue_style('social-icons' );

    wp_register_style( 'social-css', SOCIAL_NETWORK_URL.'assets/css/social-style.css', false, SOCIAL_NETWORK_VERSION );
    wp_enqueue_style('social-css' );

	if( is_admin() ) {
		// Add the color picker css file       
        wp_enqueue_style( 'wp-color-picker' ); 
        // wp_enqueue_script( 'wp-color-picker' ); 
        wp_register_script( 'social-color-picker-js', SOCIAL_NETWORK_URL.'assets/js/social-color_picker.js',array('jquery','wp-color-picker'), SOCIAL_NETWORK_VERSION );
        wp_enqueue_script('social-color-picker-js'); 

        wp_register_style( 'social-admin_style', SOCIAL_NETWORK_URL.'assets/css/social-admin_style.css',false, SOCIAL_NETWORK_VERSION );
        wp_enqueue_style('social-admin_style');
	}
}
